import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import * as Icons from '@element-plus/icons'
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

const app = createApp(App)

app.component("font-awesome-icon", FontAwesomeIcon);
// 具体引用icon的组件中

import { faVolumeUp} from "@fortawesome/free-solid-svg-icons/faVolumeUp";
import {faChevronLeft} from "@fortawesome/free-solid-svg-icons/faChevronLeft";
import {faChevronRight} from "@fortawesome/free-solid-svg-icons/faChevronRight";
import {faPieChart} from "@fortawesome/free-solid-svg-icons/faPieChart";
import {faFemale} from "@fortawesome/free-solid-svg-icons/faFemale";
import {faWrench} from "@fortawesome/free-solid-svg-icons/faWrench";
import {faHeartbeat} from "@fortawesome/free-solid-svg-icons/faHeartbeat";
import {faSearch} from "@fortawesome/free-solid-svg-icons/faSearch";
import {faServer} from "@fortawesome/free-solid-svg-icons/faServer";
import {faUser} from "@fortawesome/free-solid-svg-icons/faUser";
import {faPlay} from "@fortawesome/free-solid-svg-icons/faPlay";
/* import the fontawesome core */
import { library } from "@fortawesome/fontawesome-svg-core";

library.add(faVolumeUp,faChevronLeft,faChevronRight,faPieChart,faFemale,faWrench,faHeartbeat,faSearch,faServer,faUser,faPlay);


// 注册Icons 全局组件
Object.keys(Icons).forEach(key => {
    app.component(key, Icons[key])
})

app.use(store).use(router).use(ElementPlus).mount('#app')
