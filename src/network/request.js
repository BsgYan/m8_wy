import axios from "axios";
import router from '../router';

export function request(config) {
    const instance = axios.create({
        baseURL:'http://localhost:8081',
        timeout:5000
    })

    //请求拦截
    instance.interceptors.request.use(config=>{
        //直接放行
        return config;
    },error => {

    })

    //响应拦截
    instance.interceptors.response.use(res=>{
        return res;
    },err => {

    })




    return instance(config);
}