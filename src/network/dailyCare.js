import {request} from "./request";

// 发布服务
export function addServe(data) {
    return request({
        url:'/dailyCare/addServe',
        method:'post',
        data
    })
}
// 找所有服务
export function findServe(data) {
    return request({
        url:'/dailyCare/findServe',
        method:'post',
        data
    })
}
// 按老人id找服务
export function findServeByOid(data) {
    return request({
        url:'/dailyCare/findServeByOid',
        method:'post',
        data
    })
}
// 按亲属id找服务
export function findServeByRid(data) {
    return request({
        url:'/dailyCare/findServeByRid',
        method:'post',
        data
    })
}
// 按护工id找服务
export function findServeByWid(data) {
    return request({
        url:'/dailyCare/findServeByWid',
        method:'post',
        data
    })
}
// 按服务类型找服务
export function findServeByStclass(data) {
    return request({
        url:'/dailyCare/findServeByStclass',
        method:'post',
        data
    })
}
// 按服务名找服务
export function findServeByStname(data) {
    return request({
        url:'/dailyCare/findServeByStname',
        method:'post',
        data
    })
}
// 通过服务id找服务
export function findServeBySid(data) {
    return request({
        url:'/dailyCare/findServeBySid',
        method:'post',
        data
    })
}

// 通过stid找stclass
export function findStidByStclass(data) {
    return request({
        url:'/dailyCare/findStidByStclass',
        method:'post',
        data
    })
}

// 标题及内容模糊匹配找服务
export function findServeFuzzy(data) {
    return request({
        url:'/dailyCare/findServeFuzzy',
        method:'post',
        data
    })
}
// 查找服务类型
export function findStype(data) {
    return request({
        url:'/dailyCare/findStype',
        method:'post',
        data
    })
}
// 查找服务类型stclass
export function findStypeByStclass(data) {
    return request({
        url:'/dailyCare/findStypeByStclass',
        method:'post',
        data
    })
}
// 修改服务
export function updateServe(data) {
    return request({
        url:'/dailyCare/updateServe',
        method:'post',
        data
    })
}
// 接服务
export function pickServe(data) {
    return request({
        url:'/dailyCare/pickServe',
        method:'post',
        data
    })
}
// 拒绝接服务
export function noPick(data) {
    return request({
        url:'/dailyCare/noPick',
        method:'post',
        data
    })
}
// 指定护工
export function pickWorker(data) {
    return request({
        url:'/dailyCare/pickWorker',
        method:'post',
        data
    })
}
// 完成服务
export function finishServe(data) {
    return request({
        url:'/dailyCare/finishServe',
        method:'post',
        data
    })
}
// 评价
export function score(data) {
    return request({
        url:'/dailyCare/score',
        method:'post',
        data
    })
}
// 老人服务数量
export function oScount(data) {
    return request({
        url:'/dailyCare/oScount',
        method:'post',
        data
    })
}

// 删除
export function deleteServe(data) {
    return request({
        url:'/dailyCare/deleteServe',
        method:'post',
        data
    })
}