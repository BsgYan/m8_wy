import {request} from "./request";

// axios的传参有两种方式，一种是params，一种是data，而params会默认把 要传的参数添加到url后面

export function aLogin(data) {
    return request({
        url:'/user/aLogin',
        method:'post',
        data
    })
}

// 老人登入
export function oLogin(data) {
    return request({
        url:'/user/oLogin',
        method:'post',
        data
    })
}
// 亲属登入
export function rLogin(data) {
    return request({
        url:'/user/rLogin',
        method:'post',
        data
    })
}
// 护工登入
export function wLogin(data) {
    return request({
        url:'/user/wLogin',
        method:'post',
        data
    })
}
// 老人注册
export function oRegist(data) {
    return request({
        url:'/user/oRegist',
        method:'post',
        data
    })
}
// 亲属注册
export function rRegist(data) {
    return request({
        url:'/user/rRegist',
        method:'post',
        data
    })
}
// 护工注册
export function wRegist(data) {
    return request({
        url:'/user/wRegist',
        method:'post',
        data
    })
}
// 找所有老人
export function findOld(data) {
    return request({
        url:'/user/findOld',
        method:'post',
        data
    })
}
// 找所有亲属
export function findRelation(data) {
    return request({
        url:'/user/findRelation',
        method:'post',
        data
    })
}
// 找所有护工
export function findWorker(data) {
    return request({
        url:'/user/findWorker',
        method:'post',
        data
    })
}
// 模糊查询老人
export function findOFuzzy(data) {
    return request({
        url:'/user/findOFuzzy',
        method:'post',
        data
    })
}
// 模糊查询亲属
export function findRFuzzy(data) {
    return request({
        url:'/user/findRFuzzy',
        method:'post',
        data
    })
}
// 模糊查询护工
export function findWFuzzy(data) {
    return request({
        url:'/user/findWFuzzy',
        method:'post',
        data
    })
}
// 通过id找老人信息
export function findByOid(data) {
    return request({
        url:'/user/findByOid',
        method:'post',
        data
    })
}
// 通过id找亲属信息
export function findByRid(data) {
    return request({
        url:'/user/findByRid',
        method:'post',
        data
    })
}
// 通过id找护工信息
export function findByWid(data) {
    return request({
        url:'/user/findByWid',
        method:'post',
        data
    })
}
// 解除绑定
export function deleteUp(data) {
    return request({
        url:'/user/deleteUp',
        method:'post',
        data
    })
}
// 通过亲属id、老人电话-->老人id，绑定老人
export function bindUp(data) {
    return request({
        url:'/user/bindUp',
        method:'post',
        data
    })
}
// 通过亲属id找老人
export function findOByRid(data) {
    return request({
        url:'/user/findOByRid',
        method:'post',
        data
    })
}
// 通过老人id找亲属
export function findRByOid(data) {
    return request({
        url:'/user/findRByOid',
        method:'post',
        data
    })
}
// 修改老人信息
export function oUpdate(data) {
    return request({
        url:'/user/oUpdate',
        method:'post',
        data
    })
}
// 修改亲属信息
export function rUpdate(data) {
    return request({
        url:'/user/rUpdate',
        method:'post',
        data
    })
}
// 修改亲属信息
export function wUpdate(data) {
    return request({
        url:'/user/wUpdate',
        method:'post',
        data
    })
}

// 护工分数排序
export function orderWorker(data) {
    return request({
        url:'/user/orderWorker',
        method:'post',
        data
    })
}