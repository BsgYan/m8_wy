import {request} from "./request";

// 发布文章
export function addArticle(data) {
    return request({
        url:'/recreation/addArticle',
        method:'post',
        data
    })
}
// 发布视频
export function addVideo(data) {
    return request({
        url:'/recreation/addVideo',
        method:'post',
        data
    })
}
// 删除文章
export function deleteArticle(data) {
    return request({
        url:'/recreation/deleteArticle',
        method:'post',
        data
    })
}
// 删除视频
export function deleteVideo(data) {
    return request({
        url:'/recreation/deleteVideo',
        method:'post',
        data
    })
}
// 按类型查文章
export function findAByAtype(data) {
    return request({
        url:'/recreation/findAByAtype',
        method:'post',
        data
    })
}
// 按rid查文章
export function findAByRid(data) {
    return request({
        url:'/recreation/findAByRid',
        method:'post',
        data
    })
}
// 按aid查文章
export function findAByAid(data) {
    return request({
        url:'/recreation/findAByAid',
        method:'post',
        data
    })
}
// 按类型查视频
export function findVByVtype(data) {
    return request({
        url:'/recreation/findVByVtype',
        method:'post',
        data
    })
}
// 按rid查视频
export function findVByRid(data) {
    return request({
        url:'/recreation/findVByRid',
        method:'post',
        data
    })
}
// 按vid查视频
export function findVByVid(data) {
    return request({
        url:'/recreation/findVByVid',
        method:'post',
        data
    })
}
// 改文章
export function updateArticle(data) {
    return request({
        url:'/recreation/updateArticle',
        method:'post',
        data
    })
}
// 改视频
export function updateVideo(data) {
    return request({
        url: '/recreation/updateVideo',
        method: 'post',
        data
    })
}
// 模糊查文章
export function findArticleFuzzy(data) {
    return request({
        url: '/recreation/findArticleFuzzy',
        method: 'post',
        data
    })
}

// 模糊查视频
export function findVideoFuzzy(data) {
    return request({
        url: '/recreation/findVideoFuzzy',
        method: 'post',
        data
    })
}