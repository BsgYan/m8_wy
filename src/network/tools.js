import {request} from "./request";

// 发布服务
export function uploadFile(data) {
    return request({
        url:'/tool/uploadFile',
        method:'post',
        headers:{'Content-Type':'multipart/form-data'},
        data
    })
}