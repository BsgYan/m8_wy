import {request} from "./request";

// 反馈老人身体情况
export function addBody(data) {
    return request({
        url:'/healthCare/addBody',
        method:'post',
        data
    })
}
// 查所有反馈
export function findBody() {
    return request({
        url:'/healthCare/findBody'
    })
}
// 按反馈id查反馈
export function findBodyByBid(data) {
    return request({
        url:'/healthCare/findBodyByBid',
        method:'post',
        data
    })
}
// 按服务id查反馈
export function findBodyBySid(data) {
    return request({
        url:'/healthCare/findBodyBySid',
        method:'post',
        data
    })
}
// 按老人id查反馈
export function findBodyByOid(data) {
    return request({
        url:'/healthCare/findBodyByOid',
        method:'post',
        data
    })
}
// 按护工id查反馈
export function findBodyByWid(data) {
    return request({
        url:'/healthCare/findBodyByWid',
        method:'post',
        data
    })
}
// 修改反馈
export function updateBody(data) {
    return request({
        url:'/healthCare/updateBody',
        method:'post',
        data
    })
}
// 添加食物
export function addFood(data) {
    return request({
        url:'/healthCare/addFood',
        method:'post',
        data
    })
}
// 删除食物
export function deleteFood(data) {
    return request({
        url:'/healthCare/deleteFood',
        method:'post',
        data
    })
}
// 查所有食物
export function findFood() {
    return request({
        url:'/healthCare/findFood',
    })
}
// 按id找食物
export function findFoodByFid(data) {
    return request({
        url:'/healthCare/findFoodByFid',
        method:'post',
        data
    })
}
// 模糊查询食物、不适合、适合、食物细节
export function findFoodFuzzy(data) {
    return request({
        url:'/healthCare/findFoodFuzzy',
        method:'post',
        data
    })
}
// 修改食物
export function updateFood(data) {
    return request({
        url:'/healthCare/updateFood',
        method:'post',
        data
    })
}
// 添加eat
export function addEat(data) {
    return request({
        url:'/healthCare/addEat',
        method:'post',
        data
    })
}
// 删除eat
export function deleteEat(data) {
    return request({
        url:'/healthCare/deleteEat',
        method:'post',
        data
    })
}
// 查所有eat
export function findEat() {
    return request({
        url:'/healthCare/findEat'
    })
}
// 按老人id查eat
export function findEatByOid(data) {
    return request({
        url:'/healthCare/findEatByOid',
        method:'post',
        data
    })
}

// 按食物id查eat
export function findEatByFid(data) {
    return request({
        url:'/healthCare/findEatByFid',
        method:'post',
        data
    })
}
// 添加药品
export function addMedicine(data) {
    return request({
        url:'/healthCare/addMedicine',
        method:'post',
        data
    })
}
// 删除药品
export function deleteMedicine(data) {
    return request({
        url:'/healthCare/deleteMedicine',
        method:'post',
        data
    })
}
// 查所有药品
export function findMedicine() {
    return request({
        url:'/healthCare/findMedicine'
    })
}
// 按id找药品
export function findMedicineByMid(data) {
    return request({
        url:'/healthCare/findMedicineByMid',
        method:'post',
        data
    })
}
// 按药品名模糊查询药品
export function findMedicineFuzzy(data) {
    return request({
        url:'/healthCare/findMedicineFuzzy',
        method:'post',
        data
    })
}
// 修改药品
export function updateMedicine(data) {
    return request({
        url:'/healthCare/updateMedicine',
        method:'post',
        data
    })
}
// 添加吃药提醒
export function addMremind(data) {
    return request({
        url:'/healthCare/addMremind',
        method:'post',
        data
    })
}
// 删除吃药提醒
export function deleteMremind(data) {
    return request({
        url:'/healthCare/deleteMremind',
        method:'post',
        data
    })
}
// 查所有吃药提醒
export function findMremind() {
    return request({
        url:'/healthCare/findMremind'
    })
}
// 按药品id找用药提醒
export function findMremindBymid(data) {
    return request({
        url:'/healthCare/findMremindBymid',
        method:'post',
        data
    })
}
// 按老人id找用药提醒
export function findMremindByoid(data) {
    return request({
        url:'/healthCare/findMremindByoid',
        method:'post',
        data
    })
}
// 修改吃药提醒
export function updateMremind(data) {
    return request({
        url:'/healthCare/updateMremind',
        method:'post',
        data
    })
}