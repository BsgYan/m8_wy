import { createStore } from 'vuex'

export default createStore({
  state: {
    user:{
      isaLogin:window.localStorage.getItem('aid') ? true : false,
      isoLogin:window.localStorage.getItem('oid') ? true : false,
      isrLogin:window.localStorage.getItem('rid') ? true : false,
      iswLogin:window.localStorage.getItem('wid') ? true : false
    },
  },
  mutations: {
    setIsaLogin(state,payload){
      state.user.isaLogin=payload;
    },
    setIsoLogin(state,payload){
      state.user.isoLogin=payload;
    },
    setIsrLogin(state,payload){
      state.user.isrLogin=payload;
    },
    setIswLogin(state,payload){
      state.user.iswLogin=payload;
    },
  },
  actions: {
  },
  modules: {
  }
})
