//文字转语音
export default function Speak(text){
    speechSynthesis.cancel();
    const voice=new SpeechSynthesisUtterance();
    voice.text=text;
    voice.lang='zh';
    voice.rate=1;
    speechSynthesis.speak(voice);
}