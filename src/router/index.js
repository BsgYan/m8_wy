import { createRouter, createWebHistory } from 'vue-router'
import { ElMessage } from 'element-plus'
import store from '../store';

const admin=()=>import('views/Admin/admin')
const oldList=()=>import('views/Admin/oldList')
const relationList=()=>import('views/Admin/relationList')
const workerList=()=>import('views/Admin/workerList')
const serveList=()=>import('views/Admin/serveList')
const foodList=()=>import('views/Admin/foodList')
const medicineList=()=>import('views/Admin/medicineList')
const videoList=()=>import('views/Admin/videoList')
const articleList=()=>import('views/Admin/articleList')
const aLogin=()=>import('views/Admin/aLogin')


const oHome=()=>import('views/Old/oHome')
const oPersonal=()=>import('views/Old/oPersonal')
const oFood=()=>import('views/Old/oFood')
const oRemind=()=>import('views/Old/oRemind')
const oVideo=()=>import('views/Old/oVideo')
const oArticle=()=>import('views/Old/oArticle')
const oCheckup=()=>import('views/Old/oCheckup')
const oLifeCare=()=>import('views/Old/oLifeCare')
const oMenage=()=>import('views/Old/oMenage')
const oLogin=()=>import('views/Old/oLogin')
const oRegist=()=>import('views/Old/oRegist')


const playVideo=()=>import('views/Show/playVideo')
const playArticle=()=>import('views/Show/playArticle')


const wHome=()=>import('views/Worker/wHome')
const wPersonal=()=>import('views/Worker/wPersonal')
const wLogin=()=>import('views/Worker/wLogin')
const wRegist=()=>import('views/Worker/wRegist')


const rHome=()=>import('views/Relation/rHome')
const rHealth=()=>import('views/Relation/rHealth')
const rPersonal=()=>import('views/Relation/rPersonal')
const rPickWorker=()=>import('views/Relation/rPickWorker')
const rPickFood=()=>import('views/Relation/rPickFood')
const rPickMedicine=()=>import('views/Relation/rPickMedicine')
const rServe=()=>import('views/Relation/rServe')
const rLogin=()=>import('views/Relation/rLogin')
const rRegist=()=>import('views/Relation/rRegist')

const routes = [
  //  亲属
  {
    path: '/rLogin',
    name: 'rLogin',
    component: rLogin,
    meta:{
      title:'登入'
    }
  },
  {
    path: '/rRegist',
    name: 'rRegist',
    component: rRegist,
    meta:{
      title:'注册'
    }
  },
  {
    path: '/rHome',
    name: 'rHome',
    component: rHome,
    meta:{
      title:'首页',
      isrAuthRequired:true
    }
  },
  {
    path: '/rHealth',
    name: 'rHealth',
    component: rHealth,
    meta:{
      title:'健康中心',
      isrAuthRequired:true
    }
  },
  {
    path: '/rPersonal',
    name: 'rPersonal',
    component: rPersonal,
    meta:{
      title:'个人中心',
      isrAuthRequired:true
    }
  },
  {
    path: '/rPickWorker',
    name: 'rPickWorker',
    component: rPickWorker,
    meta:{
      title:'护工',
      isrAuthRequired:true
    }
  },
  {
    path: '/rPickFood',
    name: 'rPickFood',
    component: rPickFood,
    meta:{
      title:'食物',
      isrAuthRequired:true
    }
  },
  {
    path: '/rPickMedicine',
    name: 'rPickMedicine',
    component: rPickMedicine,
    meta:{
      title:'药品',
      isrAuthRequired:true
    }
  },
  {
    path: '/rServe',
    name: 'rServe',
    component: rServe,
    meta:{
      title:'服务中心',
      isrAuthRequired:true
    }
  },
  //  护工
  {
    path: '/wLogin',
    name: 'wLogin',
    component: wLogin,
    meta:{
      title:'登入',
    }
  },
  {
    path: '/wRegist',
    name: 'wRegist',
    component: wRegist,
    meta:{
      title:'注册',
    }
  },
  {
    path: '/wHome',
    name: 'wHome',
    component: wHome,
    meta:{
      title:'首页',
      iswAuthRequired:true
    }
  },
  {
    path: '/wPersonal',
    name: 'wPersonal',
    component: wPersonal,
    meta:{
      title:'个人中心',
      iswAuthRequired:true
    }
  },
  //  展示
  {
    path: '/playVideo',
    name: 'playVideo',
    component: playVideo,
    meta:{
      title:'视频',
      isoAuthRequired:true
    }
  },
  {
    path: '/playArticle',
    name: 'playArticle',
    component: playArticle,
    meta:{
      title:'文章',
      isoAuthRequired:true
    }
  },
  //  老人
  {
    path: '/oLogin',
    name: 'oLogin',
    component: oLogin,
    meta:{
      title:'登入'
    }
  },
  {
    path: '/oRegist',
    name: 'oRegist',
    component: oRegist,
    meta:{
      title:'注册'
    }
  },
  {
    path: '/oHome',
    name: 'oHome',
    component: oHome,
    meta:{
      title:'首页'
    }
  },
  {
    path:'/oPersonal',
    name:'oPersonal',
    component:oPersonal,
    meta:{
      title:'个人中心',
      isoAuthRequired:true
    }
  },{
    path:'/oFood',
    name:'oFood',
    component:oFood,
    meta:{
      title:'饮食建议',
      isoAuthRequired:true
    }
  },{
    path:'/oRemind',
    name:'oRemind',
    component:oRemind,
    meta:{
      title:'用药提醒',
      isoAuthRequired:true
    }
  },{
    path:'/oVideo',
    name:'oVideo',
    component:oVideo,
    meta:{
      title:'健身视频'
    }
  },{
    path:'/oArticle',
    name:'oArticle',
    component:oArticle,
    meta:{
      title:'养生文章'
    }
  },{
    path:'/oCheckup',
    name:'oCheckup',
    component:oCheckup,
    meta:{
      title:'健康护理',
      isoAuthRequired:true
    }
  },{
    path:'/oLifeCare',
    name:'oLifeCare',
    component:oLifeCare,
    meta:{
      title:'生活照料',
      isoAuthRequired:true
    }
  },{
    path:'/oMenage',
    name:'oMenage',
    component:oMenage,
    meta:{
      title:'家政服务',
      isoAuthRequired:true
    }
  },
  //  管理员
  {
    path: '/aLogin',
    name: 'aLogin',
    component: aLogin,
    meta:{
      title:'登入'
    }
  },
  {
    path: '/admin',
    name: 'admin',
    component: admin,
    meta:{
      title:'管理系统',
      isaAuthRequired:true
    },
    children:[{
      path:'/oldList',
      name:'oldList',
      component:oldList
    },{
      path:'/relationList',
      name:'relationList',
      component:relationList
    },{
      path:'/workerList',
      name:'workerList',
      component:workerList
    },{
      path:'/serveList',
      name:'serveList',
      component:serveList
    },{
      path:'/foodList',
      name:'foodList',
      component:foodList
    },{
      path:'/medicineList',
      name:'medicineList',
      component:medicineList
    },{
      path:'/videoList',
      name:'videoList',
      component:videoList
    },{
      path:'/articleList',
      name:'articleList',
      component:articleList
    }]
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

router.beforeEach((to,from,next)=>{
  //如果没有登入，在这里到login
  if(to.meta.isaAuthRequired && store.state.user.isaLogin===false){
    ElMessage({
      message: '您还没有登入',
      type: 'warning',
    })
    return next('/aLogin');
  }else if(to.meta.isoAuthRequired && store.state.user.isoLogin===false){
    ElMessage({
      message: '您还没有登入',
      type: 'warning',
    })
    return next('/oLogin');
  }else if(to.meta.isrAuthRequired && store.state.user.isrLogin===false){
    ElMessage({
      message: '您还没有登入',
      type: 'warning',
    })
    return next('/rLogin');
  }else if(to.meta.iswAuthRequired && store.state.user.iswLogin===false){
    ElMessage({
      message: '您还没有登入',
      type: 'warning',
    })
    return next('/wLogin');
  }else{
    next();
  }
  document.title=to.meta.title;
})


export default router
